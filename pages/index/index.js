// index.js
// 获取应用实例
const app = getApp()
const print = require('../../utils/print.js')

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName'), // 如需尝试获取用户信息可改为false

    // 打印相关参数begin----------
    devicesList: [],
    services: [],
    serviceId: 0,
    writeCharacter: false,
    readCharacter: false,
    notifyCharacter: false,
    isScanning: false,
    looptime: 0,
    currentTime: 1,
    lastData: 0,
    oneTimeData: 20,//发送数据大小，测试正常，不能太多
    returnResult: "returnResult",
    printNum: 3,
    currentPrint: 1,
    isReceiptSend: false,
    isLabelSend: true
    // -end----------
  },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },

  /**
   * 搜索打印机
   */
  searchBluetooth() {
    print.searchBluetooth(this);
  },

  /**
   * 选中打印机
   */
  connectBluetoothSettings(e) {
    print.connectBluetoothSettings(app, this, e);
  },

  /**
   * 打印标签
   */
  printLabText() {
    //print.labelTest(app, this);
    var print_content = new Array(8);
    print_content[0] = '同城速运210920000003';
    print_content[1] = '南郊->北郊';
    print_content[2] = '收方：尚未来 15633336666';
    print_content[3] = '收方地址：安徽合肥市中心小学';
    print_content[4] = '寄放：刘晓峰 17792002201';
    print_content[5] = '货物名称：猕猴桃 运费：100.00';
    print_content[6] = '数量：10箱';
    print_content[7] = '日期：2021-09-22 15:23:50';
    print.printLable(app, this, print_content);
  },

  onLoad() {
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
